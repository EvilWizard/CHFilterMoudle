# CHFilterMoudle

[![CI Status](http://img.shields.io/travis/coderdc/CHFilterMoudle.svg?style=flat)](https://travis-ci.org/coderdc/CHFilterMoudle)
[![Version](https://img.shields.io/cocoapods/v/CHFilterMoudle.svg?style=flat)](http://cocoapods.org/pods/CHFilterMoudle)
[![License](https://img.shields.io/cocoapods/l/CHFilterMoudle.svg?style=flat)](http://cocoapods.org/pods/CHFilterMoudle)
[![Platform](https://img.shields.io/cocoapods/p/CHFilterMoudle.svg?style=flat)](http://cocoapods.org/pods/CHFilterMoudle)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CHFilterMoudle is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CHFilterMoudle'
```

## Author

coderdc, duanchao19900812@gmail.com

## License

CHFilterMoudle is available under the MIT license. See the LICENSE file for more info.
