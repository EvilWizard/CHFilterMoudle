//
//  CHFilterMoudleModel.h
//  CHFilterMoudle
//
//  Created by 行者栖处 on 2018/2/24.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CHFilterMoudleSelectStyle) {
    CHFilterMoudleSelectStyleSingleSelection = 0,
    CHFilterMoudleSelectStyleMultiSelection  = 1,
};

typedef NS_ENUM(NSUInteger, CHFilterMoudleDataType) {
    CHFilterMoudleDataTypeSelect    = 0, // 选择
    CHFilterMoudleDataTypeInput     = 1, // 输入
    CHFilterMoudleDataTypeJump      = 2, // 跳转
};

@class CHFilterMoudleItemModel;
@interface CHFilterMoudleModel : NSObject <NSCopying>

/**
 *  是否允许收起 默认允许
 */
@property (nonatomic, assign) BOOL allowPackUp;

/**
 *  筛选要素每一组的标题
 */
@property (nonatomic, copy) NSString *sectionHeaderName;

/**
 *  关联分组（各关联分组之间单选控制时需配置）
 */
@property (nonatomic, strong) NSArray *relatedSectionNames;

/**
 *  是否展示section下每项数据(无需配置)
 */
@property (nonatomic, assign) BOOL isShowSectionItems;

/**
 *  数据类型 : 选择/输入/跳转
 */
@property (nonatomic, assign) CHFilterMoudleDataType dataTyp;

/**
 *  选择方式
 */
@property (nonatomic, assign) CHFilterMoudleSelectStyle selectStyle;

/**
 *  筛选要素每一组中子项模型数组
 */
@property (nonatomic, strong) NSArray <CHFilterMoudleItemModel *>*itemsArray;

@end

/// 筛选子项模型
@interface CHFilterMoudleItemModel : NSObject <NSCopying>

/**
 *  筛选子项名称
 */
@property (nonatomic, copy) NSString *itemName;

/**
 *  筛选子项ID
 */
@property (nonatomic, copy) NSString *itemID;

/**
 *  是否被选中(以此配置默认选中项)
 */
@property (nonatomic, assign) BOOL isSelected;

/**
 *  子项宽度(无需配置)
 */
@property (nonatomic, assign) CGFloat itemWidth;

/**
 *  是否需要跳转
 */
@property (nonatomic, assign) BOOL isNeedJump;

@end
