//
//  CHFilterMoudleCell.m
//  CHFilterMoudle_Example
//
//  Created by 行者栖处 on 2018/2/23.
//  Copyright © 2018年 coderdc. All rights reserved.
//

#import "CHFilterMoudleCell.h"
#import "CHFilterMoudleModel.h"
#import "CHFilterMoudle.h"

@interface CHFilterMoudleCell ()
@property (nonatomic, strong) UILabel *contentLabel;
@end
@implementation CHFilterMoudleCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.contentLabel];
        self.contentView.layer.cornerRadius = CHFilter_FilterItemHeight *0.5;
        self.contentView.layer.masksToBounds = YES;
        self.contentView.layer.borderWidth = 1.f;
    }
    return self;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _contentLabel.textAlignment = NSTextAlignmentCenter;
        _contentLabel.textColor = CHRGB(59, 59, 59);
        _contentLabel.font = CHFilter_FilterItemFont;
        _contentLabel.backgroundColor = CHRGB(253, 240, 217);
    }
    return _contentLabel;
}

- (void)setItemModel:(CHFilterMoudleItemModel *)itemModel {
    _itemModel = itemModel;
    self.contentLabel.frame = CGRectMake(0, 0, itemModel.itemWidth, self.bounds.size.height);
    self.contentLabel.text = itemModel.itemName;
    self.contentLabel.backgroundColor = itemModel.isSelected ? CHRGB(253, 240, 217) : CHRGB(241, 241, 248);
    self.contentView.layer.borderColor = itemModel.isSelected ? [CHRGB(245, 182, 106) CGColor] : _contentLabel.backgroundColor.CGColor;
}

@end
