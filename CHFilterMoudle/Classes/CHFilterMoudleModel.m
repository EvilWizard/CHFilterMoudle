//
//  CHFilterMoudleModel.m
//  CHFilterMoudle
//
//  Created by 行者栖处 on 2018/2/24.
//

#import "CHFilterMoudleModel.h"
#import "CHFilterMoudle.h"

@implementation CHFilterMoudleModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isShowSectionItems = YES;
        self.sectionHeaderName = @"";
        self.selectStyle = CHFilterMoudleSelectStyleMultiSelection;
        self.relatedSectionNames = nil;
        self.allowPackUp = YES;
        self.dataTyp = CHFilterMoudleDataTypeSelect;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    CHFilterMoudleModel *model = [[CHFilterMoudleModel allocWithZone:zone] init];
    model.sectionHeaderName = self.sectionHeaderName.copy;
    model.isShowSectionItems = self.isShowSectionItems;
    model.selectStyle = self.selectStyle;
    model.itemsArray = [[NSArray alloc] initWithArray:self.itemsArray copyItems:YES];
    model.relatedSectionNames = self.relatedSectionNames.copy;
    model.allowPackUp = self.allowPackUp;
    return model;
}

@end


@implementation CHFilterMoudleItemModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isSelected = NO;
        self.itemName = @"";
        self.itemWidth = 0;
        self.isNeedJump = NO;
    }
    return self;
}

- (void)setItemName:(NSString *)itemName {
    _itemName = itemName;
    if ([itemName isEqualToString:@""] || nil == itemName) {
        self.itemWidth = 0;
        return;
    }
    CGSize size = [self.itemName boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CHFilter_FilterItemHeight) options:0 attributes:@{NSFontAttributeName : CHFilter_FilterItemFont} context:0].size;
    self.itemWidth = size.width + 16;
}

- (id)copyWithZone:(NSZone *)zone {
    CHFilterMoudleItemModel *model = [[CHFilterMoudleItemModel allocWithZone:zone] init];
    model.itemName = self.itemName.copy;
    model.isSelected = self.isSelected;
    model.itemWidth = self.itemWidth;
    model.itemID = self.itemID.copy;
    model.isNeedJump = self.isNeedJump;
    return model;
}

@end
