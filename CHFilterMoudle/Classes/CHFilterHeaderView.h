//
//  CHFilterHeaderView.h
//  CHFilterMoudle_Example
//
//  Created by 行者栖处 on 2018/2/23.
//  Copyright © 2018年 coderdc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CHFilterMoudleModel,CHFilterHeaderView;
@protocol CHFilterHeaderViewDelegate <NSObject>

/**
 点击header

 @param filterHeaderView self
 @param model 选中模型
 */
- (void)filterHeaderView:(CHFilterHeaderView *)filterHeaderView didClickWithModel:(CHFilterMoudleModel *)model;
@end

@interface CHFilterHeaderView : UICollectionReusableView
/**
 *  代理
 */
@property (nonatomic, weak) id <CHFilterHeaderViewDelegate> delegate;

/**
 *  筛选模型
 */
@property (nonatomic, strong) CHFilterMoudleModel *filterModel;

@end
