//
//  CHFilterMoudleView.m
//  CHFilterMoudle_Example
//
//  Created by 行者栖处 on 2018/2/23.
//  Copyright © 2018年 coderdc. All rights reserved.
//

#import "CHFilterMoudleView.h"
#import "CHFilterMoudleCell.h"
#import "CHFilterMoudleModel.h"
#import "CHFilterHeaderView.h"
#import "CHFilterMoudle.h"
#import "CHCollectionViewFlowLayout.h"

@interface CHFilterMoudleView ()<UICollectionViewDataSource,UICollectionViewDelegate,CHFilterHeaderViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *establishButton;
@property (nonatomic, strong) UIButton *confirmButton;

@property (nonatomic, strong) UIView *backView;

@property (nonatomic, strong) NSMutableArray <CHFilterMoudleModel *>*dataSources;

@end

static NSString *CHFilterMoudleCellID = @"CHFilterMoudleCell";
static NSString *CHFilterHeaderViewID = @"CHFilterHeaderView";

@implementation CHFilterMoudleView

+ (instancetype)showFilterViewWith:(NSArray <CHFilterMoudleModel *>*)filterConditions {
    CHFilterMoudleView *filterView = [[CHFilterMoudleView alloc] initWithFrame:CGRectMake(CHFilter_ScreenWidth, 0, CHFilter_ScreenWidth *0.85, CHFilter_ScreenHeight)];
    filterView.dataSources = [filterView duplicateDataWith:filterConditions.mutableCopy];
    [CHFilter_AppKeyWindow addSubview:filterView.backView];
    [CHFilter_AppKeyWindow addSubview:filterView];
    [filterView show];
    return filterView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = CHRGB(221, 221, 221);
        [self addSubview:self.titleLabel];
        [self addSubview:self.establishButton];
        [self addSubview:self.confirmButton];
        [self addSubview:self.collectionView];
    }
    return self;
}

#pragma mark - UICollectionViewDataSource and UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataSources.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    CHFilterMoudleModel *model = self.dataSources[section];
    return model.itemsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CHFilterMoudleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CHFilterMoudleCellID forIndexPath:indexPath];
    CHFilterMoudleModel *model = self.dataSources[indexPath.section];
    CHFilterMoudleItemModel *itemModel = model.itemsArray[indexPath.item];
    cell.itemModel = itemModel;
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    CHFilterHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:CHFilterHeaderViewID forIndexPath:indexPath];
    headerView.delegate = self;
    headerView.filterModel = self.dataSources[indexPath.section];
    return headerView;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CHFilterMoudleModel *model = self.dataSources[indexPath.section];
    CHFilterMoudleItemModel *itemModel = model.itemsArray[indexPath.item];
    if (model.selectStyle == CHFilterMoudleSelectStyleSingleSelection) {
        for (int i = 0; i < model.itemsArray.count; i++) {
            CHFilterMoudleItemModel *itemModel1 = model.itemsArray[i];
            itemModel1.isSelected = NO;
        }
    }
    
    itemModel.isSelected = !itemModel.isSelected;
    [self.collectionView reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CHFilterMoudleModel *model = self.dataSources[indexPath.section];
    CHFilterMoudleItemModel *itemModel = model.itemsArray[indexPath.item];
    if (!model.isShowSectionItems) {
        return CGSizeZero;
    }
    return CGSizeMake(itemModel.itemWidth, CHFilter_FilterItemHeight);
}

#pragma mark - CHFilterHeaderViewDelegate
- (void)filterHeaderView:(CHFilterHeaderView *)filterHeaderView didClickWithModel:(CHFilterMoudleModel *)model {
    for (int i = 0; i < self.dataSources.count; i++) {
        CHFilterMoudleModel *model1 = self.dataSources[i];
        if ([model.sectionHeaderName isEqualToString:model1.sectionHeaderName]) {
            model.isShowSectionItems = !model.isShowSectionItems;
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:i]];
        }
    }
}

#pragma mark - events
- (void)show {
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = self.frame;
        frame.origin.x = CHFilter_ScreenWidth *0.15;
        self.frame = frame;
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = self.frame;
        frame.origin.x = CHFilter_ScreenWidth;
        self.frame = frame;
        self.backView.alpha = 0.f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [self.backView removeFromSuperview];
        self.backView = nil;
    }];
}

- (void)backViewDidClick {
    [self dismiss];
}

// 重置
- (void)establishButtonDidClick {
    for (int i = 0; i < self.dataSources.count; i++) {
        CHFilterMoudleModel *model = self.dataSources[i];
        for (int j = 0; j < model.itemsArray.count; j++) {
            CHFilterMoudleItemModel *itemModel = model.itemsArray[j];
            itemModel.isSelected = NO;
        }
    }
    [self.collectionView reloadData];
}

// 确认
- (void)confirmButtonDidClick {
    [self dismiss];
    if ([self.delegate respondsToSelector:@selector(filterMoudleView:didClickConfirmWith:)]) {
        [self.delegate filterMoudleView:self didClickConfirmWith:[self duplicateDataWith:self.dataSources]];
    }
}

#pragma mark - getter and setter
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        CHCollectionViewFlowLayout *layout = [[CHCollectionViewFlowLayout alloc] initWithType:AlignWithLeft betweenOfCell:5];
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), self.bounds.size.width, self.bounds.size.height - 64 - 45) collectionViewLayout:layout];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[CHFilterMoudleCell class]
            forCellWithReuseIdentifier:CHFilterMoudleCellID];
        [_collectionView registerClass:[CHFilterHeaderView class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:CHFilterHeaderViewID];
    }
    return _collectionView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 64)];
        _titleLabel.backgroundColor = CHRGB(221, 221, 221);
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.textColor = CHRGB(33, 33, 33);
        _titleLabel.text = @"  筛选";
    }
    return _titleLabel;
}

- (UIButton *)establishButton {
    if (!_establishButton) {
        _establishButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 44, self.bounds.size.width *0.5, 44)];
        [_establishButton setTitle:@"重置" forState:UIControlStateNormal];
        [_establishButton setTitleColor:CHRGB(117, 125, 127) forState:UIControlStateNormal];
        _establishButton.backgroundColor = CHRGB(241, 241, 241);
        _establishButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_establishButton addTarget:self action:@selector(establishButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _establishButton;
}

- (UIButton *)confirmButton {
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.establishButton.frame), self.bounds.size.height - 44, self.bounds.size.width *0.5, 44)];
        [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _confirmButton.backgroundColor = CHRGB(2, 169, 244);
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_confirmButton addTarget:self action:@selector(confirmButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _confirmButton;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CHFilter_ScreenWidth, CHFilter_ScreenHeight)];
        _backView.backgroundColor = [UIColor blackColor];
        _backView.alpha = 0.5f;
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backViewDidClick)];
        [_backView addGestureRecognizer:tapGes];
    }
    return _backView;
}

- (NSMutableArray *)dataSources {
    if (!_dataSources) {
        _dataSources = [NSMutableArray array];
    }
    return _dataSources;
}

- (NSMutableArray <CHFilterMoudleModel *>*)duplicateDataWith:(NSMutableArray <CHFilterMoudleModel *>*)filterModels {
    NSMutableArray *models = [NSMutableArray array];
    for (int i = 0; i < filterModels.count; i++) {
        CHFilterMoudleModel *model1 = filterModels[i];
        CHFilterMoudleModel *model = [[CHFilterMoudleModel alloc] init];
        NSMutableArray *itemsArray = [NSMutableArray array];
        model.sectionHeaderName = model1.sectionHeaderName.copy;
        model.isShowSectionItems = model1.isShowSectionItems;
        model.selectStyle = model1.selectStyle;
        model.allowPackUp = model1.allowPackUp;
        NSArray <CHFilterMoudleItemModel *>*items = model1.itemsArray;
        for (int j = 0; j < items.count; j++) {
            CHFilterMoudleItemModel *itemModel1 = items[j];
            CHFilterMoudleItemModel *itemModel = [[CHFilterMoudleItemModel alloc] init];
            itemModel.itemName = itemModel1.itemName.copy;
            itemModel.isSelected = itemModel1.isSelected;
            itemModel.itemWidth = itemModel1.itemWidth;
            [itemsArray addObject:itemModel];
        }
        model.itemsArray = itemsArray.copy;
        [models addObject:model];
    }
    return models;
}

- (void)dealloc {
    [self.dataSources removeAllObjects];
}

@end
