//
//  CHFilterMoudleCell.h
//  CHFilterMoudle_Example
//
//  Created by 行者栖处 on 2018/2/23.
//  Copyright © 2018年 coderdc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CHFilterMoudleItemModel;
@interface CHFilterMoudleCell : UICollectionViewCell

@property (nonatomic, strong) CHFilterMoudleItemModel *itemModel;

@end
