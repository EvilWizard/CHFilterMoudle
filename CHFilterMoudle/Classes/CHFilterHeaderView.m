//
//  CHFilterHeaderView.m
//  CHFilterMoudle_Example
//
//  Created by 行者栖处 on 2018/2/23.
//  Copyright © 2018年 coderdc. All rights reserved.
//

#import "CHFilterHeaderView.h"
#import "CHFilterMoudle.h"
#import "CHFilterMoudleModel.h"

@interface CHFilterHeaderView ()
@property (nonatomic, strong) UILabel *headerTitleLabel;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIButton *arrowButton;
@end
@implementation CHFilterHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.headerTitleLabel];
        [self addSubview:self.lineView];
        [self addSubview:self.arrowButton];
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerViewDidClick:)];
        [self addGestureRecognizer:tapGes];
    }
    return self;
}

- (void)headerViewDidClick:(UITapGestureRecognizer *)tapGes {
    if ([self.delegate respondsToSelector:@selector(filterHeaderView:didClickWithModel:)]) {
        [self.delegate filterHeaderView:self didClickWithModel:self.filterModel];
    }
}

- (UILabel *)headerTitleLabel {
    if (!_headerTitleLabel) {
        _headerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.bounds.size.width - 10, self.bounds.size.height)];
        _headerTitleLabel.textColor = [UIColor blackColor];
        _headerTitleLabel.textAlignment = NSTextAlignmentLeft;
        _headerTitleLabel.font = [UIFont systemFontOfSize:CHFilter_ScreenWidth > 320 ? 15 : 14];
    }
    return _headerTitleLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame) - CHPIX(1), CGRectGetWidth(self.frame), CHPIX(1))];
        _lineView.backgroundColor = CHRGB(243, 242, 241);
    }
    return _lineView;
}

- (UIButton *)arrowButton {
    if (!_arrowButton) {
        _arrowButton = [[UIButton alloc] initWithFrame:CGRectMake(self.bounds.size.width - 25, (CHFilter_FilterHeaderHeight - 15) *0.5, 15, 15)];
        _arrowButton.selected = NO;
        NSBundle *bundle = [NSBundle bundleForClass:self.class];
        NSString *arrowDownPath = [bundle pathForResource:@"CHFilterMoudle.bundle/arrow-down" ofType:@"png"];
        NSString *arrowUpPath = [bundle pathForResource:@"CHFilterMoudle.bundle/arrow-up" ofType:@"png"];
        [_arrowButton setImage:[UIImage imageWithContentsOfFile:arrowDownPath] forState:UIControlStateNormal];
        [_arrowButton setImage:[UIImage imageWithContentsOfFile:arrowUpPath] forState:UIControlStateSelected];
    }
    return _arrowButton;
}

- (void)setFilterModel:(CHFilterMoudleModel *)filterModel {
    _filterModel = filterModel;
    self.headerTitleLabel.text = filterModel.sectionHeaderName;
    self.arrowButton.selected = !filterModel.isShowSectionItems;
    self.arrowButton.hidden = !filterModel.allowPackUp;
    self.userInteractionEnabled = filterModel.allowPackUp;
}

@end
