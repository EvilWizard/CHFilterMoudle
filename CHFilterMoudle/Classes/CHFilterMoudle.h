//
//  CHFilterMoudle.h
//  Pods
//
//  Created by 行者栖处 on 2018/2/24.
//

#define CHFilter_AppKeyWindow [UIApplication sharedApplication].keyWindow
#define CHFilter_FilterItemHeight 30
#define CHFilter_FilterHeaderHeight 44
#define CHFilter_ScreenWidth [UIScreen mainScreen].bounds.size.width
#define CHFilter_ScreenHeight [UIScreen mainScreen].bounds.size.height
#define CHFilter_FilterItemFont [UIFont fontWithName:@"HelveticaNeue" size:CHFilter_ScreenWidth > 320 ? 13 : 12]
#define  CHRGB(r,g,b)  [UIColor colorWithRed:((r)/255.0) green:((g)/255.0) blue:((b)/255.0) alpha:1]
#define  CHRGBA(r,g,b,a)  [UIColor colorWithRed:((r)/255.0) green:((g)/255.0) blue:((b)/255.0) alpha:(alpha)]
#define CHPIX(i) (i / [UIScreen mainScreen].scale)
