//
//  CHFilterMoudleView.h
//  CHFilterMoudle_Example
//
//  Created by 行者栖处 on 2018/2/23.
//  Copyright © 2018年 coderdc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CHFilterMoudleModel,CHFilterMoudleView;

@protocol CHFilterMoudleViewDelegate<NSObject>

/**
 点击确认回调

 @param filterView self
 @param filterResult 筛选结果
 */
- (void)filterMoudleView:(CHFilterMoudleView *)filterView didClickConfirmWith:(NSArray <CHFilterMoudleModel *>*)filterResult;

@end
@interface CHFilterMoudleView : UIView

/**
 初始化一个筛选视图,并show

 @param filterConditions 筛选条件
 @return 筛选视图
 */
+ (instancetype)showFilterViewWith:(NSArray <CHFilterMoudleModel *>*)filterConditions;

/**
 *  代理
 */
@property (nonatomic, weak) id <CHFilterMoudleViewDelegate> delegate;

@end
