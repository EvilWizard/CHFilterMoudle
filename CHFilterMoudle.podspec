Pod::Spec.new do |s|
  s.name             = 'CHFilterMoudle'
  s.version          = '0.1.12'
  s.summary          = 'A filterView use waterfall'
  s.description      = <<-DESC
  A filterView like JD use collectionView
                       DESC
  s.homepage         = 'https://gitlab.com/EvilWizard/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'EvilWizard' => 'duanchao19900812@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/EvilWizard/CHFilterMoudle.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'CHFilterMoudle/Classes/**/*'
  s.resource_bundles = {
  'CHFilterMoudle' => ['CHFilterMoudle/Assets/*.png']
  }
end
