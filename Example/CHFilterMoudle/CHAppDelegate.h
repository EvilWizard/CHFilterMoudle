//
//  CHAppDelegate.h
//  CHFilterMoudle
//
//  Created by coderdc on 02/23/2018.
//  Copyright (c) 2018 coderdc. All rights reserved.
//

@import UIKit;

@interface CHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
