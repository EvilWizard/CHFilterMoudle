//
//  CHViewController.m
//  CHFilterMoudle
//
//  Created by coderdc on 02/23/2018.
//  Copyright (c) 2018 coderdc. All rights reserved.
//

#import "CHViewController.h"
#import "CHFilterMoudleView.h"
#import "CHFilterMoudle.h"
#import "CHFilterMoudleModel.h"

@interface CHViewController ()<CHFilterMoudleViewDelegate>

@property (nonatomic, strong) NSArray <NSDictionary *>*dataSource;

@property (nonatomic, strong) UILabel *contentLabel;

// 配置筛选条件要用全局变量存储
@property (nonatomic, strong) NSArray *filterConditions;

@end

@implementation CHViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.filterConditions = [self filterMoudleModels];
    [self.view addSubview:self.contentLabel];
}

- (NSArray *)dataSource {
    if (!_dataSource) {
        _dataSource = @[@{@"供应商" : @[@"空间配送",@"yuzai配送",@"配送中心",@"东城配送",@"南城配送中心",@"配送中心附属",@"北京一品海鲜批发部"]}, @{@"仓库" : @[@"点线面馆111",@"面点间",@"吧台",@"凉菜间",@"点线面管东城门店点线面管东城门店",@"烤"]}];
    }
    return _dataSource;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    CHFilterMoudleView *view = [CHFilterMoudleView showFilterViewWith:self.filterConditions];
    view.delegate = self;
}

#pragma mark - CHFilterMoudleViewDelegate
- (void)filterMoudleView:(CHFilterMoudleView *)filterView didClickConfirmWith:(NSArray <CHFilterMoudleModel *>*)filterResult {
    NSLog(@"%@",filterResult);
    NSMutableString *s = [NSMutableString string];
    
    for (int i = 0; i < filterResult.count; i++) {
        CHFilterMoudleModel *model = filterResult[i];
        [s appendFormat:@"\n\n%@:\n",model.sectionHeaderName];
        NSArray *items = model.itemsArray;
        for (int i = 0; i < items.count; i++) {
            CHFilterMoudleItemModel *itemModel = items[i];
            if (itemModel.isSelected) {
                [s appendFormat:@"%@\t",itemModel.itemName];
            }
        }
    }
    self.filterConditions = filterResult;
    self.contentLabel.text = s.copy;
}

- (NSArray <CHFilterMoudleModel *>*)filterMoudleModels {
    NSMutableArray <CHFilterMoudleModel *>*filterModels = [NSMutableArray array];
    for (int i = 0; i < self.dataSource.count; i++) {
        NSDictionary *dict = self.dataSource[i];
        CHFilterMoudleModel *model = [[CHFilterMoudleModel alloc] init];
        model.allowPackUp = NO;
        model.sectionHeaderName = dict.allKeys.firstObject;
        if (i == 1) {
            model.selectStyle = CHFilterMoudleSelectStyleSingleSelection;
        }
        else {
            model.selectStyle = CHFilterMoudleSelectStyleMultiSelection;
        }
        NSArray *array = dict.allValues.firstObject;
        NSMutableArray <CHFilterMoudleItemModel *>*items = [NSMutableArray array];
        for (int i = 0; i < array.count; i++) {
            CHFilterMoudleItemModel *model = [[CHFilterMoudleItemModel alloc] init];
            model.itemName = array[i];
            [items addObject:model];
        }
        model.itemsArray = items.copy;
        [filterModels addObject:model];
    }
    return filterModels.copy;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, CHFilter_ScreenWidth - 20, CHFilter_ScreenHeight - 200)];
        _contentLabel.textAlignment = NSTextAlignmentLeft;
        _contentLabel.textColor = CHRGB(33, 33, 33);
        _contentLabel.font = CHFilter_FilterItemFont;
        _contentLabel.userInteractionEnabled = NO;
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
